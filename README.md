**Các chức năng của project**

- Người dùng chạy ứng dụng, chương trình hiển thị màn hình menu yêu cầu người dùng chọn chức năng cần sử dụng. 
- Tiếp đó, chương trình thực hiện chức năng đã được lựa chọn và đưa ra kết quả sau đó trở về menu.
- Bắt buộc phải viết 2 lớp TaskType và Task với các thuộc tính đã cho ở trên.

**Chức năng 1:** Thêm Loại task

Màn hình chức năng như trên cho phép người dùng nhập tên loại task. Mã loại Task phải đúng theo yêu cầu bẳng mã loại task cuối cùng cộng thêm 1 nếu chưa có mã loại task thì giá trị mã loại task bằng 1.

**Chức năng 2:** Sửa Loại task

Cho phép người dùng nhập mã loại task sau đó hiển thị tên của mã loại task và cho phép người dùng nhập tên loại task cần sửa và update vào danh sách.

**Chức năng 3:** Xóa loại task.

Cho phép người dùng nhập mã loại task sau đó xóa loại task này ra khỏi danh sách. Nếu thành công hay thất bại thì phải thông báo cho người dùng biết.

**Chức năng 4:** Hiển thị loại task.

**Chức năng 5:** Thêm task.

- Đầu vào:
- requirementName: Tên requirement
- assignee: Tên người nhận task.
- reviewer: Tên người kiểm tra task.
- TaskTypeId: id loại task. (có thể viết menu cho phép người dùng chọn TaskType)
- date: Ngày thực hiện task.
- planFrom: thời gian bắt đầu.
- planTo: thời gian kết thúc.

Xử lý nếu người dùng nhập sai dữ liệu thì phải cho người dùng nhập lại dữ liệu đó cho đến lúc đúng

**Chức năng 6:** Tìm kiếm

Người dùng có thể tìm kiếm theo tên task hoặc taskType, nếu không muốn tìm nữa thì lúc tìm kiếm không cần nhập vào ô tìm kiếm

**Chức năng 7**: Hiển thì danh sách taskID và TaskTypeID

Khi nhấn vào Edit hoặc Delete thì sẽ có hiện danh sách ID để người dùng thao tác.

**Cấu hình project để chạy:**

- Thay đổi Server trong DefaultConnection string bên trong file appsetting.json.

- Cài đặt các package cần thiếu nếu thiếu: EntityFrameWork, Tool, Sql,..
- Thực hiện “add-magration initial” trong lần chạy đầu tiên.
- Thực hiện “update-database” trong lần chạy đầu tiên.
- Nhấn F5 để chạy project và dữ liệu mẫu sẽ được sinh ra tự động.

**Topic Used:**
- Repository
- Unit of Work
- Abstract, Interface, Generic, readonly, nullable,...
- DbContext, Code First
- Data annotation configuration
- Fluent API Configuration
- Logging
- Migration & revert migration
- LINQ to entity, insert, update, delete, querying,...
- Related Entities
- Eager Loading
