﻿using MiniProjectEFNhaNVT12.Models;
using System;
using System.Linq;

namespace MiniProjectEFNhaNVT12.Data
{
    public static class DbInitializer
    {
        public static void Initialize(TaskContext context)
        {
            context.Database.EnsureCreated();

            //look for any tasks.
            if (context.Taskks.Any() && context.TaskTypes.Any())
                {
                return; //Db has been seed
            }
            var tasktypes = new TaskType[]
            {
                new TaskType(){ TaskTypeId = 1, Name = "Most Important" },
                new TaskType(){  TaskTypeId = 2,Name = "Important" },
                new TaskType(){  TaskTypeId = 3,Name = "Less Important" },
            };
            foreach (var taskType in tasktypes)
            {
                context.TaskTypes.Add(taskType);           
            }
            context.SaveChanges();
            var taskks = new Taskk[]
            {
                new Taskk(){
                     TaskTypeId = 1 , RequirementName =" learn SQL", Reviewer ="Bang",
                    Assignee=" Nha", Date=DateTime.Parse("2022-01-01"), PlanFrom=8.0, PlanTo=17.0},
                new Taskk(){
                     TaskTypeId = 2 , RequirementName =" learn C#", Reviewer ="Thang",
                    Assignee=" Duy", Date=DateTime.Parse("2022-02-01"), PlanFrom=8.5, PlanTo=16.0},
                new Taskk(){
                     TaskTypeId = 1 , RequirementName =" learn MVC", Reviewer ="Nhan",
                    Assignee=" Sinh", Date=DateTime.Parse("2022-03-02"), PlanFrom=9.0, PlanTo=14.0},
            };
            foreach (var taskk in taskks)
            {
                context.Taskks.Add(taskk);
            }
            context.SaveChanges();

        }
    }
}
