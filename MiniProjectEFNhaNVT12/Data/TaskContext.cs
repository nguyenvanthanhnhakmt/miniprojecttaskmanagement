﻿using Microsoft.EntityFrameworkCore;
using MiniProjectEFNhaNVT12.Models;
using System;
using System.Linq;

namespace MiniProjectEFNhaNVT12.Data
{
    public class TaskContext : DbContext
    {
       
        public TaskContext(DbContextOptions<TaskContext> options) : base(options)
        {

        }
        public DbSet<Taskk> Taskks { get; set; }
        public DbSet<TaskType> TaskTypes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Fluent API
            modelBuilder.Entity<Taskk>().ToTable("Taskk");
            modelBuilder.Entity<TaskType>().ToTable("TaskType");
            modelBuilder.Entity<Taskk>().Property(e => e.Assignee).IsUnicode(true);
            modelBuilder.Entity<Taskk>().Property(e => e.Reviewer).IsUnicode(true);
            modelBuilder.Entity<Taskk>().Property(e => e.RequirementName).IsUnicode(true);

            //Shadow Property
            var allEntities = modelBuilder.Model.GetEntityTypes();

            foreach( var entity in allEntities)
            {
                entity.AddProperty("CreatedDate", typeof(DateTime));
                entity.AddProperty("UpdatedDate", typeof (DateTime));
            }
        }
        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);
            foreach (var entityEntry in entries)
            {
                entityEntry.Property("UpdatedDate").CurrentValue = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    entityEntry.Property("CreatedDate").CurrentValue = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }

    }
}

