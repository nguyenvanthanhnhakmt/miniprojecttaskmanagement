﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MiniProjectEFNhaNVT12.Data;
using MiniProjectEFNhaNVT12.Models;
using MiniProjectEFNhaNVT12.UnitOfWork;

namespace MiniProjectEFNhaNVT12.Controllers
{
    public class TaskksController : Controller
    {
        private readonly TaskContext _context;
        private readonly ILogger _logger;
        public string Message { get; set; }

        private IUnitOfWork _unitOfWork;
        public TaskksController(TaskContext context, IUnitOfWork unitOfWork, ILogger<TaskksController> logger)
        {
            _context = context;
            this._unitOfWork = unitOfWork;
            _logger = logger;
        }


        // GET: Taskks
        public async Task<IActionResult> Index()
        {
            var taskContext = _context.Taskks.Include(t => t.TaskType);
            return View(await taskContext.ToListAsync());
        }
        [HttpGet]
        public IActionResult GetIdTask(bool isEdit)
        {
            var miniDbContext = _unitOfWork.repositoryTask.GetAllTask();

            if (isEdit)
                return PartialView("CheckIdTask", miniDbContext);
            else
                return PartialView("CheckIdTaskDelete", miniDbContext);
        }

        // GET: Tasks/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var task = _unitOfWork.repositoryTask.GetById(id);
            if (task == null)
            {
                return NotFound();
            }

            return View(task);
        }

        // GET: Taskks/Create
        public IActionResult Create()
        {
            ViewData["TaskTypeId"] = new SelectList(_context.TaskTypes, "TaskTypeId", "TaskTypeId");
            return View();
        }

        // POST: Taskks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TaskkId,TaskTypeId,RequirementName,Date,PlanFrom,PlanTo,Assignee,Reviewer")] Taskk taskk)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.repositoryTask.Create(taskk);
                _unitOfWork.Save();

                Message = $"Create task {taskk.RequirementName} at {DateTime.UtcNow.ToLongTimeString()}";

                _logger.LogInformation(MyLogEvents.InsertItem ,Message);

                return RedirectToAction(nameof(Index));
            }
            ViewData["TaskTypeId"] = new SelectList(_context.TaskTypes, "TaskTypeId", "TaskTypeId", taskk.TaskTypeId);
            return View(taskk);
        }

        // GET: Taskks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskk = _unitOfWork.repositoryTask.GetById(id);
            if (taskk == null)
            {
                return NotFound();
            }
            ViewData["TaskTypeId"] = new SelectList(_context.TaskTypes, "TaskTypeId", "TaskTypeId", taskk.TaskTypeId);
            return View(taskk);
        }

        // POST: Taskks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TaskkId,TaskTypeId,RequirementName,Date,PlanFrom,PlanTo,Assignee,Reviewer")] Taskk taskk)
        {
            if (id != taskk.TaskkId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _unitOfWork.repositoryTask.Update(taskk);
                    _unitOfWork.Save();
                    
                    Message = $"Edit task {taskk.RequirementName} at {DateTime.UtcNow.ToLongTimeString()}";

                    _logger.LogInformation(MyLogEvents.UpdateItem, Message);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TaskkExists(taskk.TaskkId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TaskTypeId"] = new SelectList(_context.TaskTypes, "TaskTypeId", "TaskTypeId", taskk.TaskTypeId);
            return View(taskk);
        }

        // GET: Taskks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskk = _unitOfWork.repositoryTask.GetById(id);
                
            if (id == null)
            {
                return NotFound();
            }

            return View(taskk);
        }

        // POST: Taskks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var taskk = _unitOfWork.repositoryTask.GetById(id);
            _unitOfWork.repositoryTask.Delete(taskk);
            _unitOfWork.Save();

            Message = $"Delete task {taskk.RequirementName} at {DateTime.UtcNow.ToLongTimeString()}";

            _logger.LogInformation(MyLogEvents.DeleteItem, Message);

            return RedirectToAction(nameof(Index));
        }

        private bool TaskkExists(int id)
        {
            return _context.Taskks.Any(e => e.TaskkId == id);
        }
    }
}
