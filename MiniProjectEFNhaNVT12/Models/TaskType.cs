﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MiniProjectEFNhaNVT12.Models
{
    public class TaskType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TaskTypeId { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        public ICollection<Taskk> Taskk { get; set; }
    }
}

