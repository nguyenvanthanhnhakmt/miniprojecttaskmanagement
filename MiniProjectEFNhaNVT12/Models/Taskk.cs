﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MiniProjectEFNhaNVT12.Models
{
    public class Taskk
    {
        public int TaskkId { get; set; }
        [Required]
        public int TaskTypeId { get; set; }

        [Required]
        public string RequirementName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [Range(8, 17.5, ErrorMessage = "Value for Plan From must be between {0}h and {1}h.")]
        public double? PlanFrom { get; set; }

        [Range(8, 17.5, ErrorMessage = "Value for Plan To must be between {0}h and {1}h.")]
        public double? PlanTo { get; set; }

        public string Assignee { get; set; }
        
        public string Reviewer { get; set; }

        public TaskType TaskType { get; set; }

    }
}
