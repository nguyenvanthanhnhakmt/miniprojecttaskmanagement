﻿using MiniProjectEFNhaNVT12.Models;
using System.Collections.Generic;

namespace MiniProjectEFNhaNVT12.Repository
{
    public interface IRepository<T>
    {
        void Update(T entity);
        void Delete(T entity);
        void Create(T entity);
        T GetById(int? iDentity);
        IList<T> GetAll();
        IList<Taskk> GetAllTask();
    }
}
