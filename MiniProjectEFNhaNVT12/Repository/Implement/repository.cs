﻿using Microsoft.EntityFrameworkCore;
using MiniProjectEFNhaNVT12.Data;
using MiniProjectEFNhaNVT12.Models;
using MiniProjectEFNhaNVT12.Repository;
using System.Collections.Generic;
using System.Linq;

namespace MiniProjectEFNhaNVT12.Implement.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {

        public DbSet<T> db;
        private readonly TaskContext _context;

        public Repository(TaskContext context)
        {
            _context = context;
            db = _context.Set<T>();

        }

        // method update entity
        public void Update(T entity)
        {
            db.Update(entity);
        }

        // method delete entity
        public void Delete(T entity)
        {
            db.Remove(entity);
        }

        // method create entity
        public void Create(T entity)
        {
            db.Add(entity);
        }

        // method get entity by id
        public T GetById(int? iDentity)
        {
            return db.Find(iDentity);
        }

        // get all list entity 
        public IList<T> GetAll()
        {
            return db.ToList();
        }
        public IList<Taskk> GetAllTask()
        {
            return _context.Taskks.Include(x => x.TaskType).ToList();
        }
    }
}

