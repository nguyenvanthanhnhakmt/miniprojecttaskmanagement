﻿using MiniProjectEFNhaNVT12.Models;
using MiniProjectEFNhaNVT12.Repository;

namespace MiniProjectEFNhaNVT12.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<Taskk> repositoryTask { get; }
        IRepository<TaskType> repositoryTaskType { get; }
        void Save();

    }
}
