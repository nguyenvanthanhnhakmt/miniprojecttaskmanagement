﻿using MiniProjectEFNhaNVT12.Data;
using MiniProjectEFNhaNVT12.Implement.Repository;
using MiniProjectEFNhaNVT12.Models;
using MiniProjectEFNhaNVT12.Repository;
using MiniProjectEFNhaNVT12.UnitOfWork;

namespace MiniProjectEFNhaNVT12.Implement.UnitOfWork
{
    public class unitOfWork : IUnitOfWork
    {
        private readonly TaskContext _context;

        public unitOfWork(TaskContext context)
        {
            _context = context;
        }
        public IRepository<Taskk> repositoryTask
        {
            get
            {
                return new Repository<Taskk>(_context);
            }
        }

        public IRepository<TaskType> repositoryTaskType
        {
            get
            {
                return new Repository<TaskType>(_context);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}

