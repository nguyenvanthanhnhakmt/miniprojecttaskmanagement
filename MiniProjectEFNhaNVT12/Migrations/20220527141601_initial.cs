﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MiniProjectEFNhaNVT12.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskType",
                columns: table => new
                {
                    TaskTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskType", x => x.TaskTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Taskk",
                columns: table => new
                {
                    TaskkId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TaskTypeId = table.Column<int>(nullable: false),
                    RequirementName = table.Column<string>(nullable: false),
                    Date = table.Column<DateTime>(nullable: true),
                    PlanFrom = table.Column<double>(nullable: true),
                    PlanTo = table.Column<double>(nullable: true),
                    Assignee = table.Column<string>(nullable: true),
                    Reviewer = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taskk", x => x.TaskkId);
                    table.ForeignKey(
                        name: "FK_Taskk_TaskType_TaskTypeId",
                        column: x => x.TaskTypeId,
                        principalTable: "TaskType",
                        principalColumn: "TaskTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Taskk_TaskTypeId",
                table: "Taskk",
                column: "TaskTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Taskk");

            migrationBuilder.DropTable(
                name: "TaskType");
        }
    }
}
